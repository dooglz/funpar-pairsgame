package pairsGame.Client

import org.jcsp.groovy.*
import org.jcsp.lang.*

class Matcher implements CSProcess {
	ChannelInput getValidPoint
	ChannelOutput validPoint
	ChannelInput receivePoint
	ChannelOutput getPoint
	
	def getXY = { point, side, gap ->
		def int x = (point[0] - gap) / (side + gap)
		def int y = (point[1] - gap) / (side + gap)
		return [x, y]
	}
	
	void run (){
		def mainAlt = new ALT([getValidPoint, receivePoint])
		def gotValidPoint = false
		def pairsMap = []
		def side = 50
		def gap = 5
		def pointXY
		while (true){
			switch ( mainAlt.select() )
			{
				case 0:
					//request from player
					
					def getData = (S_GetValidPoint)getValidPoint.read()
					//get the state of the map
				//	println "Matcher reads player"
					pairsMap = getData.pairsMap
					side = getData.side
					gap = getData.gap
					break;
				case 1:
					//update from mousebuffer
					def point = ((MousePoint)receivePoint.read()).point
					//println "Matcher reads buffer"
					def newpointXY = getXY(point, side, gap)
					//If the point is the same as the last point, ignore it
					if(!pairsMap.empty && pointXY != newpointXY){
						pointXY = newpointXY
						gotValidPoint = pairsMap.containsKey(pointXY)
						if(gotValidPoint){
						//	println "Matcher attempts to write to player2"
							validPoint.write(new S_SquareCoords(location: pointXY))
						}
					}else{
						gotValidPoint = false
					}
					break;
			}
		} // end while
		
	} // end run
}
