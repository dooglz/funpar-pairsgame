package pairsGame.Client
@SuppressWarnings('DuplicateNumberLiteral')
@SuppressWarnings('DuplicateStringLiteral')
@SuppressWarnings('SpaceAroundOperator')
@SuppressWarnings('SpaceAfterIf')
@SuppressWarnings('SpaceAfterComma')
@SuppressWarnings('SpaceAfterClosingBrace')
@SuppressWarnings('SpaceBeforeOpeningBrace')
@SuppressWarnings('Println')
@SuppressWarnings('ClosureStatementOnOpeningLineOfMultipleLineClosure')

import org.jcsp.awt.DisplayList
import org.jcsp.awt.GraphicsCommand
import org.jcsp.groovy.ALT
import org.jcsp.groovy.ChannelOutputList
import org.jcsp.lang.CSProcess
import org.jcsp.lang.ChannelInput
import org.jcsp.lang.ChannelOutput
import org.jcsp.net2.tcpip.TCPIPNodeAddress
import org.jcsp.net2.Node
import org.jcsp.net2.NetChannel
import pairsGame.Shared.S_ClaimPair
import pairsGame.Shared.S_EnrolDetails
import pairsGame.Shared.S_EnrolPlayer
import pairsGame.Shared.S_FlipTile
import pairsGame.Shared.S_GameDetails
import pairsGame.Shared.S_WithdrawFromGame
import java.security.SecureRandom
import java.awt.Color

class PlayerManager implements CSProcess {
	DisplayList dList
	ChannelOutputList playerNames
	ChannelOutputList pairsWon
	ChannelOutput IPlabel
	ChannelInput IPfield
	ChannelOutput IPconfig
	ChannelInput withdrawButton
	ChannelInput nextButton
	ChannelOutput getValidPoint
	ChannelInput validPoint
	ChannelOutput nextPairConfig
	ChannelOutput turnConfig

	GraphicsCommand[] display

	int side = 50
	int gridsize = 10
	int minPairs = 5
	int maxPairs = 10
	int gap = 5
	def offset = [gap, gap]
	int graphicsPos = (side / 2)
	def rectSize = ((side + gap) * 10) + gap
	def defaultIP = "146.176.165.87"

	void run() {
		display = new GraphicsCommand[(5*gridsize*gridsize) + 4]

		def inputport = Math.abs(new SecureRandom().nextInt() % 1000) + 4000

		UpdateGrid(null)
		dList.set(display)
		IPconfig.write(inputport)
		IPlabel.write('What is your name?')
		def playerName = IPfield.read()
		IPconfig.write(defaultIP)
		IPlabel.write('What is the IP address of the game controller?')
		def controllerIP = IPfield.read().trim()
		IPconfig.write(' ')
		IPlabel.write('Connecting to the GameController')

		// create Node and Net Channel Addresses
		println "port: ${inputport}"
		def nodeAddr = new TCPIPNodeAddress (inputport)
		Node.instance.init (nodeAddr)
		def toControllerAddr = new TCPIPNodeAddress ( controllerIP, 3000)
		def toController = NetChannel.any2net(toControllerAddr, 50 )
		def fromController = NetChannel.net2one()
		def fromControllerLoc = fromController.location

		def selectAlt = new ALT([fromController, withdrawButton,validPoint])
		def UPDATE = 0
		def WITHDRAW = 1
		def VALIDPOINT = 2

		// connect to game controller
		IPconfig.write('Now Connected - sending your name to Controller')
		def enrolPlayer = new S_EnrolPlayer( name: playerName, toPlayerChannelLocation: fromControllerLoc)
		toController.write(enrolPlayer)
		def enrolDetails = (S_EnrolDetails)fromController.read()
		def myPlayerId = enrolDetails.id
		def enroled = true

		if (myPlayerId == -1) {
			enroled = false
			IPlabel.write('Sorry ' + playerName + ', there are too many players enroled in this PAIRS game')
			IPconfig.write('  Please close the game window')
			return
		}

		IPlabel.write('Hi ' + playerName + ', you are now enroled in the PAIRS game')
		IPconfig.write(' ')
		def gameId

		def currentPair = 0 //how many tiles we have selcted so far
		def pairsMap = null
		def chosenPairs = [null, null]

		def IDLE = 0
		def SELECT = 1
		def gamestate = IDLE

		while (enroled) {
			println "\nloop! state: ${gamestate}"
			if (pairsMap != null) {
				getValidPoint.write (new S_GetValidPoint( side: side, gap: gap, pairsMap: pairsMap))
			}
			println 'switching'
			switch ( selectAlt.select() ) { // ALT([fromController, withdrawButton,validPoint])
				case WITHDRAW:
					println 'quitting'
					withdrawButton.read()
					toController.write(new S_WithdrawFromGame(id: myPlayerId))
					enroled = false
					break
				case UPDATE:
					println 'Update from server'
					def gameDetails = (S_GameDetails)fromController.read()
					gameId = gameDetails.gameId
					def whosTurn = gameDetails.turnId
					IPconfig.write('Playing Game Number - ' + gameId)
					def playerMap = gameDetails.playerDetails
					pairsMap = gameDetails.pairsSpecification
					def playerIds = playerMap.keySet()

				//Update UI
					for ( i in 0..<playerNames.size()) {
						playerNames[i].write("-")
						pairsWon[i].write("  -")
					}

					playerIds.each { p ->
						def pData = playerMap.get(p)
						playerNames[p].write(pData[0])
						pairsWon[p].write(" " + pData[1])
					}

					UpdateGrid(pairsMap)

				//Is it our turn?
					if(whosTurn == myPlayerId){
						println "It's Our Turn!"
						gamestate = SELECT
					}else{
						//lockout ui
						chosenPairs = [null, null]
						currentPair = 0
					}
					if(playerMap.size() > 0 && playerMap.containsKey(whosTurn)){
						turnConfig.write('Player [' + whosTurn + ']  - '+ playerMap.get(whosTurn)[0] + 's turn')
					}

					println "Idle end - our ID ${myPlayerId}, turn ${whosTurn}"
					break
				case VALIDPOINT:
					println 'Mouse Press'
					def vPoint = ((S_SquareCoords)validPoint.read()).location
					if (gamestate == SELECT){
						//fires when mouse pressed on a tile.
						//add tile to chosenPairs array
						chosenPairs[currentPair] = vPoint
						currentPair = currentPair + 1

						//update UI, highlighting selected tile
						def pairData = pairsMap.get(vPoint)
						changePairs(vPoint[0], vPoint[1], pairData[1], pairData[0])

						//Have we chosen 2 tiles yet?
						if (chosenPairs[1] != null){
							println 'sending selection to server'
							//send our choice to the server
							toController.write(new S_ClaimPair ( id: myPlayerId,gameId: gameId,p1: chosenPairs[0],p2: chosenPairs[1]))
							gamestate = IDLE
							chosenPairs = [null, null]
							currentPair = 0
						}else{
							//flip a tile
							println 'sending flip to server'
							toController.write(new S_FlipTile ( id: myPlayerId,gameId: gameId,p1: vPoint))
						}
					}
					break
			}//end alt switch
			println 'select end'
		} //end while(enrolled)
		IPlabel.write('Goodbye ' + playerName + ', please close game window')
	} // end run

	//--------------- functions
	def UpdateGrid = {pairsMap ->
		display[0] = new GraphicsCommand.SetColor(Color.WHITE)
		display[1] = new GraphicsCommand.FillRect(0, 0, rectSize, rectSize)
		display[2] = new GraphicsCommand.SetColor(Color.BLACK)
		display[3] = new GraphicsCommand.DrawRect(0, 0, rectSize, rectSize)
		def cg = 4
		for ( x in 0..(gridsize-1)) {
			for ( y in 0..(gridsize-1)) {
				int xPos = offset[0] + (gap * x) + (side * x)
				int yPos = offset[1] + (gap * y) + (side * y)
				//print " $x, $y, $xPos, $yPos, $cg, "
				if(pairsMap?.containsKey([x,y])) {
					if(	pairsMap.get([x,y])[2] == true){
						display[cg] = new GraphicsCommand.SetColor(	pairsMap.get([x,y])[1])
					} else {
						display[cg] = new GraphicsCommand.SetColor(Color.LIGHT_GRAY)
					}
				}else{
					display[cg] = new GraphicsCommand.SetColor(Color.WHITE)
				}
				cg = cg + 1
				display[cg] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
				cg = cg + 1
				display[cg] = new GraphicsCommand.SetColor(Color.BLACK)
				cg = cg + 1
				display[cg] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
				cg = cg + 1
				xPos = xPos + graphicsPos
				yPos = yPos + graphicsPos
				if(pairsMap?.containsKey([x,y])) {
					if(	pairsMap.get([x,y])[2] == true) {
						display[cg] = new GraphicsCommand.DrawString(' ' + pairsMap.get([x,y])[0],xPos, yPos)
					} else {
						display[cg] = new GraphicsCommand.DrawString('#',xPos, yPos)
					}
				} else {
					display[cg] = new GraphicsCommand.DrawString(' ',xPos, yPos)
				}
				cg = cg + 1
			}
		}
		dList.change(display, 0)
	}

	//Changes the attributes of a single rendered tile
	def changePairs = {x, y, colour, p ->
		int xPos = offset[0] + (gap*x) + (side*x)
		int yPos = offset[1] + (gap*y) + (side*y)
		GraphicsCommand[] changeGraphics = new GraphicsCommand[5]
		changeGraphics[0] = new GraphicsCommand.SetColor(colour)
		changeGraphics[1] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
		changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
		changeGraphics[3] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
		xPos = xPos + graphicsPos
		yPos = yPos + graphicsPos
		if ( p > -1){
			changeGraphics[4] = new GraphicsCommand.DrawString(" " + p, xPos, yPos)
		}else{
			changeGraphics[4] = new GraphicsCommand.DrawString("   ", xPos, yPos)
		}
		dList.change(changeGraphics, 4 + (x * 50) + (y*5))
	}

	//NP = number of pairs
	def createPairs = {np ->
		def rng = new SecureRandom()
		for (p in 1..np) {

			//Find A random tile
			def x1 = rng.nextInt(gridsize)
			def y1 = rng.nextInt(gridsize)
			while ( pairsMap.get([x1,y1]) != null) {
				//println "first repeated random location [$x1, $y1]"
				x1 = rng.nextInt(gridsize)
				y1 = rng.nextInt(gridsize)
			}

			//find another random empty tile
			def x2 = rng.nextInt(gridsize)
			def y2 = rng.nextInt(gridsize)
			while ( x2 != x1 && y2 != y1 && pairsMap.get([x2,y2]) != null) {
				//println "second repeated random location [$x2, $y2]"
				x2 = rng.nextInt(gridsize)
				y2 = rng.nextInt(gridsize)
			}

			//place the tiles into the map
			pairsMap.put([x1, y1], [p, colours[p % 4]])
			pairsMap.put([x2, y2], [p, colours[p % 4]])
		}
	} // end createPairs

	def pairsMatch = {pairsMap, cp ->
		// cp is a list comprising two elements each of which is a list with the [x,y]
		// location of a sqaure
		// returns 0 if only one square has been chosen so far
		//         1 if the two chosen squares have the same value (and colour)
		//         2 if the chosen sqaures have different values
		if (cp[1] == null) {
			return 0
		}
		def p1Data = pairsMap.get(cp[0])
		def p2Data = pairsMap.get(cp[1])
		return ((p1Data[0] == p2Data[0]) ? 1 : 2)
	}

}
