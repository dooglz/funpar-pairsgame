package pairsGame.Client

import org.jcsp.groovy.*
import org.jcsp.lang.*

import java.awt.event.*

class MouseBuffer implements CSProcess {
	ChannelInput mouseEvent
	ChannelInput getPoint
	ChannelOutput sendPoint
	
	void run(){
		def alt = new ALT([getPoint, mouseEvent])
		def preCon = new boolean[2]
		def GET = 0
		preCon[GET] = false
		preCon[1] = true
		def point
		
		while (true){
			switch ( alt.select(preCon)) {
				case GET :
					//getPoint.read()
					//sendPoint.write(new MousePoint (point: point))
					//preCon[GET] = false //don't fire multiple times
					break
				case 1: // mouse event
					def mEvent = mouseEvent.read()
					if (mEvent.getID() == MouseEvent.MOUSE_PRESSED) {
						//loop round 
						preCon[GET] = true
						def pointValue = mEvent.getPoint()
						point = [(int)pointValue.x, (int)pointValue.y]
						//println "Buffer attempts to write to matcher"
						sendPoint.write(new MousePoint (point: point))
					}
					break
			} // end of switch
		} // end while
	} // end run
}
