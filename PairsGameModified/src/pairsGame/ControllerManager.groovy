package pairsGame
@SuppressWarnings('DuplicateNumberLiteral')
@SuppressWarnings('DuplicateStringLiteral')
@SuppressWarnings('SpaceAroundOperator')
@SuppressWarnings('SpaceAfterIf')
@SuppressWarnings('SpaceAfterComma')
@SuppressWarnings('SpaceAfterClosingBrace')
@SuppressWarnings('SpaceBeforeOpeningBrace')
@SuppressWarnings('Println')
@SuppressWarnings('ClosureStatementOnOpeningLineOfMultipleLineClosure')

import org.jcsp.awt.DisplayList
import org.jcsp.awt.GraphicsCommand
import org.jcsp.groovy.ChannelOutputList
import org.jcsp.lang.CSProcess
import org.jcsp.lang.ChannelOutput
import org.jcsp.net2.tcpip.TCPIPNodeAddress
import org.jcsp.net2.Node
import org.jcsp.net2.NetChannel
import java.security.SecureRandom
import java.awt.Color

import pairsGame.Shared.S_ClaimPair
import pairsGame.Shared.S_EnrolDetails
import pairsGame.Shared.S_EnrolPlayer
import pairsGame.Shared.S_FlipTile
import pairsGame.Shared.S_GameDetails
import pairsGame.Shared.S_WithdrawFromGame

class ControllerManager implements CSProcess{
	DisplayList dList
	ChannelOutput IPlabelConfig
	ChannelOutput statusConfig
	ChannelOutput pairsConfig
	ChannelOutput turnConfig
	ChannelOutputList playerNames
	ChannelOutputList pairsWon

	GraphicsCommand[] display

	int gridsize = 10
	def pairsMap = [:]
	def colours = [Color.MAGENTA, Color.CYAN, Color.YELLOW, Color.PINK]
	int maxPlayers = 8
	int side = 50
	int minPairs = 10
	int maxPairs = 20
	int graphicsPos = (side / 2)
	int gap = 5
	def offset = [gap, gap]
	def rectSize = ((side+gap) *10) + gap

	void run(){

		int pairsRange = maxPairs - minPairs
		def availablePlayerIds = ((maxPlayers-1) .. 0).collect{it}
		def usedPlayerIds = []

		display = new GraphicsCommand[(5*gridsize*gridsize) + 4]

		// create a Node and the fromPlayers net channel
		def nodeAddr = new TCPIPNodeAddress (3000)
		Node.getInstance().init (nodeAddr)
		IPlabelConfig.write(nodeAddr.getIpAddress())
		//println "Controller IP address = ${nodeAddr.getIpAddress()}"

		def fromPlayers = NetChannel.net2one()
		//println "Controller: fromPlayer channel location - ${fromPlayersLoc.toString()}"

		def toPlayers = new ChannelOutputList()
		for ( p in 0..<maxPlayers){ toPlayers.append(null)}
		def currentPlayerId = 0
		def playerMap = [:]

		def nPairs = 0
		def pairsUnclaimed = 0
		def gameId = 0
		def turnId = 0
		def timer = new Timer()

		def UpdatePlayers = {
			for(chan in usedPlayerIds) {
				print "Update ${chan}.. "
				toPlayers[chan].write(new S_GameDetails( playerDetails: playerMap, pairsSpecification: pairsMap, gameId: gameId, turnId: turnId))
			}
			println "Updated players"
		}

		while (true) {
			statusConfig.write("Creating")
			nPairs = generatePairsNumber(minPairs, pairsRange)
			pairsUnclaimed = nPairs
			pairsConfig.write(" "+ nPairs)
			gameId = gameId + 1
			createPairs (nPairs)
			UpdateGrid(pairsMap)
			dList.set(display)

			statusConfig.write("Running")
			def running = (pairsUnclaimed != 0)
			UpdatePlayers()
			while (running){
				UpdateGrid(pairsMap)
				println "\nloop! turn:${turnId}, players:{$usedPlayerIds.size}"
				if(playerMap.size() > 0 && playerMap.containsKey(turnId)){
					turnConfig.write(" "+ turnId + " "+ playerMap.get(turnId)[0])
				}
				//Wait for message from player
				def o = fromPlayers.read()
				
				//New player
				if ( o instanceof S_EnrolPlayer) {
					def playerDetails = (S_EnrolPlayer)o
					def playerName = playerDetails.name
					def playerToAddr = playerDetails.toPlayerChannelLocation
					def playerToChan = NetChannel.one2net(playerToAddr)
					println "new player!: ${playerDetails.name}"
					if (availablePlayerIds.size() > 0) {
						currentPlayerId = availablePlayerIds. pop()
						usedPlayerIds.add((Integer)currentPlayerId)
						playerNames[currentPlayerId].write(playerName)
						pairsWon[currentPlayerId].write(' 0')
						toPlayers[currentPlayerId] = playerToChan
						toPlayers[currentPlayerId].write(new S_EnrolDetails(id: currentPlayerId) )
						playerMap.put(currentPlayerId, [playerName, 0]) // [name, pairs claimed]
						println "${playerDetails.name} assigned ID: ${currentPlayerId}"
					}
					else {
						// no new players can join the game
						playerToChan.write(new S_EnrolDetails(id: -1))
					}
				//player flipped a tile
				} else if ( o instanceof S_FlipTile) {
					def tile = (S_FlipTile)o
					def gameNo = tile.gameId
					if ( gameId == gameNo){
						def id = tile.id
						def p1 = tile.p1
						println "player[$id] attempting to flip tile $p1"
						if(turnId != id){
							//Not your turn !
							println "but it wasn't player[$id] turn!"
						}else if ((pairsMap.get(p1) != null)){
							println "flipped"
							pairsMap.get(p1)[2] = true
						}else{
							//uh-oh
							println "but the tile doesn't exist!"
						}
					}
					
				//player tried to claim a pair
				}else if ( o instanceof S_ClaimPair) {
					def claimPair = (S_ClaimPair)o
					def gameNo = claimPair.gameId
					if ( gameId == gameNo){
						def id = claimPair.id
						def p1 = claimPair.p1
						def p2 = claimPair.p2
						println "player[$id] attempting to claim pair $p1, $p2"
						if(turnId != id){
							//Not your turn !
							println "but it wasn't player[$id] turn!"
						}
						//Is this a valid pair?
						else if ((pairsMap.get(p1) != null) && (pairsMap.get(p2) != null)
						&& (pairsMap.get(p1)[0] == pairsMap.get(p2)[0])
						&& (pairsMap.get(p1)[1] == pairsMap.get(p2)[1]))
						{
							println "The pair is valid!"
							//It's valid!
							pairsMap.remove(p2)
							pairsMap.remove(p1)
							//getplayer
							def playerState = playerMap.get(id)
							//++score
							playerState[1] = playerState[1] + 1
							//update server UI
							pairsWon[id].write(" " + playerState[1])
							//update player stats
							playerMap.put(id, playerState)
							pairsUnclaimed = pairsUnclaimed - 1
							running = (pairsUnclaimed != 0)
						}else {
							println "The pair was not valid :("
							//Invalid Pair, next player
							//show the tiles
							if(pairsMap.get(p1) != null){
								pairsMap.get(p1)[2] = true
							}
							if(pairsMap.get(p2) != null){
								pairsMap.get(p2)[2] = true
							}
							
							//wait 1.5 seconds then reset tiles and go to next player
							def originator = nextPlayer(turnId, usedPlayerIds)
							timer.runAfter(1500) {
								if(pairsMap.get(p1) != null){
									pairsMap.get(p1)[2] = false
								}
								if(pairsMap.get(p2) != null){
									pairsMap.get(p2)[2] = false
								}
								turnId = originator
								
								if(!usedPlayerIds.contains(turnId)){
									turnId = nextPlayer(turnId, usedPlayerIds)
								}
								
								UpdatePlayers()
							}
							turnId = -1
						}
					}
					
				//player leaving game
				} else {
					def withdraw = (S_WithdrawFromGame)o
					def id = withdraw.id
					def playerState = playerMap.get(id)
					println "Player: ${playerState[0]} claimed ${playerState[1]} pairs"
					playerNames[id].write("       ")
					pairsWon[id].write("   ")
					toPlayers[id] = null
					availablePlayerIds << id
					availablePlayerIds =  availablePlayerIds.sort().reverse()
					usedPlayerIds = usedPlayerIds - ((Integer) id)
					playerMap.remove(id);
					//was it their turn?
					if(turnId == id){
						ResetFlips()
						turnId = nextPlayer(turnId, usedPlayerIds)
					}
				} // end else if chain

				//If we are here, then something has happened, update players
				UpdatePlayers()

			} // while running
		} // end while true
	} // end run

	//------------------- Functions

	Integer nextPlayer(Integer current, ArrayList playerlist){
		int index = playerlist.indexOf(current)
		if (index == playerlist.size()-1){
			index = 0
		}else{
			index ++
		}
		return playerlist[index]
	} //
	
	void ResetFlips(){
		pairsMap.each { k,v ->
			v[2] = false
		}
	}
	
	def UpdateGrid = {pairsMap ->
		display[0] = new GraphicsCommand.SetColor(Color.WHITE)
		display[1] = new GraphicsCommand.FillRect(0, 0, rectSize, rectSize)
		display[2] = new GraphicsCommand.SetColor(Color.BLACK)
		display[3] = new GraphicsCommand.DrawRect(0, 0, rectSize, rectSize)
		int cg = 4
		for ( x in 0..(gridsize-1)) {
			for ( y in 0..(gridsize-1)) {
				int xPos = offset[0]+(gap*x)+ (side*x)
				int yPos = offset[1]+(gap*y)+ (side*y)
				//print " $x, $y, $xPos, $yPos, $cg, "
				if(pairsMap?.containsKey([x,y])) {
					if(pairsMap.get([x,y])[2] == true){
						display[cg] = new GraphicsCommand.SetColor(	Color.green)
					}else{
						display[cg] = new GraphicsCommand.SetColor(	pairsMap.get([x,y])[1])
					}
				}else{
					display[cg] = new GraphicsCommand.SetColor(Color.WHITE)
				}
				cg = cg+1
				display[cg] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
				cg = cg+1
				display[cg] = new GraphicsCommand.SetColor(Color.BLACK)
				cg = cg+1
				display[cg] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
				cg = cg+1
				xPos = xPos + graphicsPos
				yPos = yPos + graphicsPos
				if(pairsMap?.containsKey([x,y])) {
					display[cg] = new GraphicsCommand.DrawString(" "+pairsMap.get([x,y])[0],xPos, yPos)
				}else{
					display[cg] = new GraphicsCommand.DrawString("   ",xPos, yPos)
				}
				cg = cg+1
			}
		}
		dList.change(display, 0)
	}

	//Changes the attributes of a single rendered tile
	def changePairs = {x, y, colour, p ->
		int xPos = offset[0]+(gap*x)+ (side*x)
		int yPos = offset[1]+(gap*y)+ (side*y)
		GraphicsCommand[] changeGraphics = new GraphicsCommand[5]
		changeGraphics[0] = new GraphicsCommand.SetColor(colour)
		changeGraphics[1] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
		changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
		changeGraphics[3] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
		xPos = xPos + graphicsPos
		yPos = yPos + graphicsPos
		if ( p > -1){
			changeGraphics[4] = new GraphicsCommand.DrawString(" " + p, xPos, yPos)
		}else{
			changeGraphics[4] = new GraphicsCommand.DrawString("   ", xPos, yPos)
		}
		dList.change(changeGraphics, 4 + (x*50) + (y*5))
	}

	//NP = number of pairs
	def createPairs = {np ->
		def rng = new SecureRandom()
		for (p in 1..np){

			//Find A random tile
			def x1 = rng.nextInt(10)
			def y1 = rng.nextInt(10)
			while ( pairsMap.get([x1,y1]) != null){
				//println "first repeated random location [$x1, $y1]"
				x1 = rng.nextInt(10)
				y1 = rng.nextInt(10)
			}

			//find another random empty tile
			def x2 = rng.nextInt(10)
			def y2 = rng.nextInt(10)
			while ( x2 != x1 && y2 != y1 && pairsMap.get([x2,y2]) != null){
				//println "second repeated random location [$x2, $y2]"
				x2 = rng.nextInt(10)
				y2 = rng.nextInt(10)
			}

			//place the tiles into the map
			pairsMap.put([x1, y1], [p, colours[p%4],false])
			pairsMap.put([x2, y2], [p, colours[p%4],false])
		}
	} // end createPairs

	def generatePairsNumber = { min, range ->
		def rng = new SecureRandom()
		def randomAmount = rng.nextInt(range)
		return min + randomAmount
	}

}
