package pairsGame.Shared

//sent from player to server, when the player wants to claim a pair.
class S_ClaimPair implements Serializable {
	def gameId
	def id
	def p1
	def p2
}
