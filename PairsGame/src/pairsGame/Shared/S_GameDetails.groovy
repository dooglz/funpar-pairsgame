package pairsGame.Shared

import java.io.Serializable;

//sent from server to player, when the player requests with a S_GetGameDetails

class S_GameDetails implements Serializable {
	def gameId
	//A map of players, Key = player ID, Data = [Player Name,Score]
	def playerDetails = null
	
	//Map, Key = [x1, y1], Value =  [unique number, colour])
	def pairsSpecification = null
}
