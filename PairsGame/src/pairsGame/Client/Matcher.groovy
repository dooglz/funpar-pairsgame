package pairsGame.Client

import org.jcsp.groovy.*
import org.jcsp.lang.*

class Matcher implements CSProcess {
	ChannelInput getValidPoint
	ChannelOutput validPoint
	ChannelInput receivePoint
	ChannelOutput getPoint
	
	def getXY = { point, side, gap ->
		def int x = (point[0] - gap) / (side + gap)
		def int y = (point[1] - gap) / (side + gap)
		return [x, y]
	}
	
	void run (){
		while (true){
			//get the state of the map
			def getData = (S_GetValidPoint)getValidPoint.read()
			def pairsMap = getData.pairsMap
			def side = getData.side
			def gap = getData.gap
			//println  "Matcher - $side, $gap"
			//pairsMap.each {println"${it}"}
			
			//wait for a mouseclick, check if valid.
			def gotValidPoint = false
			def pointXY 
			while (!gotValidPoint){
				getPoint.write(0)
				//fired only on click
				def point = ((MousePoint)receivePoint.read()).point
				pointXY = getXY(point, side, gap)
				//println  "point = $point; pointXY = $pointXY"
				if ( pairsMap.containsKey(pointXY)) gotValidPoint = true
			}
			
			//println  "Matcher: pointXY = $pointXY"
			validPoint.write(new S_SquareCoords(location: pointXY))
		} // end while
		
	} // end run
}
